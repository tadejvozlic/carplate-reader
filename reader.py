import os
import time
import math
import json
import subprocess
import requests 
import multiprocessing 

from picamera import PiCamera
from time import sleep

webhook = 'https://hooks.slack.com/services/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'
david_plate = 'lj438ik'
camera = PiCamera()

def notify(message):
    data = {
        'text': message,
    } 
    # sending post request and saving response as response object 
    r = requests.post(url = webhook, data = json.dumps(data),  headers={'Content-Type': 'application/json'}) 

def check_plates(plate):
    with open('plates.json' ) as file:
        plates = json.load(json_file)["plates"]
    for element in plates:
        print(element)
        if(element.lower() == plate.lower()):
            return true
    return false

def take_image():
    i = 0
    while i < 30:
        sleep(2)
        ts = math.floor(time.time() + (60 * 60))
        camera.capture('./images/' + str(ts) + '.jpg')

        p = multiprocessing.Process(target=process_image, args=("./images/" + str(ts) + ".jpg",))
        p.start()
        i += 1
def process_image(img):
    # return
        data = subprocess.run(["alpr", "-j", "-c", "eu", img], stdout=subprocess.PIPE).stdout.decode('utf-8')
        result = json.loads(data)["results"]
        if len(result) > 0:
            # print(result)
            print(result[0]["plate"])
            if david_plate.lower() == result[0]["plate"].lower():
                notify("David gre!")
            if check_plates(result[0]["plate"]):
                notify("Redarji!")
        os.remove(img)
        print("processing finished")

def start():
    camera.start_preview()
    take_image()
    camera.stop_preview()
start()
    # alpr -j -c eu MVIMG_20180704_101923.jpg 
